from modules_dir.module import DetectModule
import re
import OpenSSL.crypto


class Module(DetectModule):
    """
    Module for identification IBANs in the file

    """

    DEFAULT_CONFIG = {
        'name': 'iban',
        'description': 'Finds ibans in the file',
        'status': 'stable',
        'regex': r"[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}[a-zA-Z0-9]{0,16}",
        'type': 'file'
    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)

    def run(self, artifact_file):
        # for file-type modules, handle the whole file

        with open(artifact_file) as f:
                return re.findall(self.config['regex'], f.read(), re.MULTILINE)