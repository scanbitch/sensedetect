from modules_dir.module import DetectModule


class Module(DetectModule):
    """
    Example module for printing all the output
    """
    DEFAULT_CONFIG = {
        'name': 'linprint',
        'description': 'Default plugin to return True to all input artifacts',
        'status': 'alpha',
        'type': 'line',

    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)

    def run(self, artifact):
        # main implementation

        return True
