from modules_dir.module import DetectModule
import re


class Module(DetectModule):
    """
    Example module for printing all the output
    """
    DEFAULT_CONFIG = {
        'name': 'email',
        'description': 'Finds emails',
        'status': 'stable',
        'email_regex': r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)",
        'type': 'token'

    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)

    def run(self, artifact):
        # main implementation

        if re.match(self.config['email_regex'], artifact) is not None:
            return True

        return False
