from modules_dir.module import DetectModule
import re
import OpenSSL.crypto


class Module(DetectModule):
    """
    Module for identification of PEM certificates in the file

    Original REGEX:

    (?<=-----BEGIN CERTIFICATE-----)(?:\S+|\s(?!-----END CERTIFICATE-----))+(?=\s-----END CERTIFICATE-----)

    """

    DEFAULT_CONFIG = {
        'name': 'pem',
        'description': 'Finds PEM certificates in the file',
        'status': 'stable',
        'regex': r"(-----BEGIN CERTIFICATE-----)(?:|(?!-----END CERTIFICATE-----))([^\-]+)(-----END CERTIFICATE-----)",
        'type': 'file',
        'convert': False
    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)

    def run(self, artifact_file):
        # for file-type modules, handle the whole file

        with open(artifact_file) as f:
            if self.config['convert']:
                return self._convert_cert(re.findall(self.config['regex'], f.read(), re.MULTILINE))

            return self._merge_certs(re.findall(self.config['regex'], f.read(), re.MULTILINE))

    def _convert_cert(self, cert_array):
        # convert x509 to text

        res_cert_array = list()

        for cert in cert_array:
            cert = ''.join(cert)

            c = OpenSSL.crypto
            try:
                cert_obj = c.load_certificate(c.FILETYPE_PEM, cert)

                res_cert_array.append(
                    "{0}, {1}, {2}".format(cert_obj.get_serial_number(), cert_obj.get_issuer(),
                                           cert_obj.get_subject())
                )

            except:
                res_cert_array.append(cert)

        return res_cert_array

    def _merge_certs(self, cert_array):
        # build the cert from the regex findall

        res_cert_array = list()

        for cert in cert_array:
            cert = ''.join(cert)

            res_cert_array.append(cert)

        return res_cert_array
