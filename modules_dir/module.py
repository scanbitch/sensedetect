class DetectModule:
    """
    Parent class for all detection modules - handles configuration overrides
    """

    def __init__(self, config_override="", DEFAULT_CONFIG="", READ_ONLY_CONFIG=""):
        self.config = DEFAULT_CONFIG

        if config_override:
            # overrida config from the main configuration file

            if self.config['name'] in config_override:
                # we have configuration to override
                for cfg_over_key, cfg_over_value in config_override[self.config['name']].items():
                    if cfg_over_key in READ_ONLY_CONFIG:
                        # skip the read_only values for override
                        continue

                    # override the config
                    self.config[cfg_over_key] = cfg_over_value

    def get_name(self):
        return self.config['name']

    def __getattr__(self, item):
        if item in self.config:
            return self.config[item]
        else:
            return False

    def __str__(self):
        return 'Obj_{0}'.format(self.name)

    def run(self):
        # method to be overriden by the modules
        pass