from modules_dir.module import DetectModule


class Module(DetectModule):
    """
    Module to lookup passwords in dictionary
    """

    DEFAULT_CONFIG = {
        'name': 'pwd_dict',
        'description': 'Finds password from dictionary',
        'status': 'stable',
        'type': 'token',
        'password_dictionary': '/Users/lukasfutera/Documents/repos/futik/sensedetect/passwords.txt',
        'minimal_length': 8

    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override="", meta=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)
        self.meta = meta

        self.password_dictionary = None

        self._load_password_dictionoary()

    def run(self, artifact):
        # main implementation

        if len(artifact) >= self.config['minimal_length'] and artifact in self.password_dictionary:
            return True

        return False

    def _load_password_dictionoary(self):
        self.password_dictionary = set()

        try:
            pf = open(self.config['password_dictionary'], 'r', encoding="latin-1", errors="surrogateescape")

            for password in pf.readlines():
                self.password_dictionary.add(password[:-1])

        except:
            self.password_dictionary = set()
