from modules_dir.module import DetectModule
import re
import traceback


class Module(DetectModule):
    """
    Module for identification of CC

    regex downloaded from: https://www.regular-expressions.info/creditcard.html

    ^(?:4[0-9]{12}(?:[0-9]{3})?                  # Visa
             |  (?:5[1-5][0-9]{2}                # MasterCard
             | 222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}
             |  3[47][0-9]{13}                   # American Express
             |  3(?:0[0-5]|[68][0-9])[0-9]{11}   # Diners Club
             |  6(?:011|5[0-9]{2})[0-9]{12}      # Discover
             |  (?:2131|1800|35\d{3})\d{11}      # JCB
    )$

    """

    DEFAULT_CONFIG = {
        'name': 'cc',
        'description': 'Finds credit-cards',
        'status': 'stable',
        'regex': r"^(?:4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\d{3})\d{11})$",
        'type': 'token',
        'luhn': True

    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)

    def run(self, artifact):
        # main implementation

        if re.match(self.config['regex'], artifact) is not None:
            if self.config['luhn']:
                return self.is_luhn_valid(artifact)

            return True

        return False

    def _luhn_checksum(self, card_number):
        def digits_of(n):
            return [int(d) for d in str(n)]

        digits = digits_of(card_number)
        odd_digits = digits[-1::-2]
        even_digits = digits[-2::-2]
        checksum = 0
        checksum += sum(odd_digits)
        for d in even_digits:
            checksum += sum(digits_of(d * 2))
        return checksum % 10

    def is_luhn_valid(self, card_number):
        return self._luhn_checksum(card_number) == 0
