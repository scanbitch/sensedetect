from modules_dir.module import DetectModule
import string


class Module(DetectModule):
    """
    Module to identify passwords via policy definition
    """

    DEFAULT_CONFIG = {
        'name': 'pwd_policy',
        'description': 'Identifies potential passwords by a policy',
        'status': 'stable',
        'type': 'token',
        'policy': [8, 1, 1, 1]  # length, uppercase, number, special

    }

    READ_ONLY_CONFIG = ('name', 'description')

    def __init__(self, config_override="", meta=""):
        # default initialization
        DetectModule.__init__(self, config_override, self.DEFAULT_CONFIG, self.READ_ONLY_CONFIG)
        self.meta = meta

        self.password_policy = self.config['policy']

    def run(self, artifact):
        # main implementation

        return self._cmp_policies(self.password_policy, self._identify_policy(artifact))

    def _identify_policy(self, artifact):
        # analyse the string and return its policy
        length = len(artifact)
        upper = sum(1 for c in artifact if c.isupper())
        number = sum(1 for c in artifact if c.isdigit())
        special = sum(1 for c in string.punctuation if c in artifact)

        return [length, upper, number, special]

    def _cmp_policies(self, policy1, policy2):
        # compare two policies.

        policy_match = 0

        for policy_index in range(0, len(policy1)):
            # iterate through items

            if policy1[policy_index] == policy2[policy_index]:
                policy_match += 1

        if policy_match == len(policy1):
            return True

        return False
