"""
Main component executing the sensitive detector tool

"""

import sys
import os
import argparse
import yaml

from log import LogFile
from modules import Modules

YML_CONFIG_DEFAULT = './detect.yml'


def argument_parser():
    # configure the argument parser

    parser = argparse.ArgumentParser(description='SenseDetect: Sensitive data log detector.')
    group = parser.add_mutually_exclusive_group()

    parser.add_argument('-m', '--modules', metavar='N', type=str, nargs='+',
                        help='Module / modules to execute on the logfile')

    group.add_argument('-f', '--log-file', type=str,
                        help='Log file to parse')

    group.add_argument('-l', '--list-modules', action="store_true",
                        help='List available modules')

    parser.add_argument('-c', '--config-file', type=str,
                        help='Use config file')

    return parser


def config_parser(yml_config_file=""):
    # parse the configuration overrides in the local directory

    config_dict = {}

    if not yml_config_file:
        yml_config_file = YML_CONFIG_DEFAULT

    if os.path.isfile(yml_config_file):
        # parse the config file
        try:
            yml_config_file = open(yml_config_file)
        except:
            print('File {0} can not be opened '.format(yml_config_file))
            sys.exit(-1)

        try:
            config_dict = yaml.load(yml_config_file)
        except yaml.YAMLError:
            print('could not parser yaml in {0}'.format(yml_config_file))

        return config_dict


if __name__ == '__main__':
    # main def of the runner process

    # parse the command line arguments
    parser = argument_parser()
    args = parser.parse_args()

    # parse the config file
    config = config_parser(args.config_file)

    # initialize the modules
    modules = Modules(config)

    # filter to select utilised modules
    modules_filter = None

    if args.modules:
        # if modules specified in command line arguments, they take precedence
        modules_filter = args.modules
        if 'all' in args.modules:
            modules_filter = None
    else:
        # otherwise check if there are modules selected in the configuration file
        if modules in config.keys() and config['modules']:
            modules_filter = config['modules']

    # list the modules and exit - if asked
    if args.list_modules:
        modules.list_modules()
        sys.exit(0)

    if not args.log_file and not args.list_modules:
        # log-file parameter is not provided - print help and exit
        parser.print_help()
        print('\n\n{0}: log file has to be provided'.format(sys.argv[0]))
        sys.exit(-1)

    # instantiate the main LogFile object - responsible to parse the log and execute modules
    lf = LogFile(args.log_file, config, modules_filter, modules)

    # initialize log - ready to be processed
    lf.init_log()

    # parse the logs - the results are stored in the object
    lf.parse_log()

    # print the results
    lf.get_results()
