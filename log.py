import csv
import json
import re


class LogFile:
    """
    Class to work with the logfile
    """

    DEFAULT_DELIMITER = " "
    DEFAULT_QUOTECHAR = "\""

    def __init__(self, logfile, config="", modules_filter="", modules=""):
        # constructor of LogFile class

        self.modules = modules.get_config()
        self.config = config
        self.logfile = logfile

        self.modules_filter = modules_filter

        if 'delimiter' in self.config:
            self.delimiter = self.config['delimiter']
        else:
            self.delimiter = self.DEFAULT_DELIMITER

        if 'quoutechar' in self.config:
            self.quotechar = self.config['quotechar']
        else:
            self.quotechar = self.DEFAULT_QUOTECHAR

        self.log_handler = None

        self.results = dict()

    ########## Handle logs

    def init_log(self):
        # open the log file and prepare for iterations - use CSV reader
        self.log_handler = csv.reader(open(self.logfile, 'r'), quotechar=self.quotechar, delimiter=self.delimiter)

    def parse_log(self):
        # go through the log and execute modules

        line_counter = 0
        results_line = list()
        results_token = list()

        for csv_line in self.log_handler:
            # iterate through CSV lines

            add_line_to_result = False

            # line counter for reporting purposes
            line_counter += 1

            result_tmp = {
                'line_number': line_counter
            }

            line = self.delimiter.join(csv_line)
            line_tokenised = csv_line

            # execute the line-based modules
            line_results = self._handle_line(line)

            if line_results:
                # report the results, if any

                result_line_tmp = result_tmp

                result_line_tmp['line'] = line
                result_line_tmp['modules'] = line_results
                results_line.append(result_line_tmp)

            # exceute the token-based modules
            token_results = self._handle_tokens(line_tokenised)

            if token_results:
                # handle token results
                result_token_tmp = result_tmp

                result_token_tmp['tokens'] = token_results

                results_token.append(result_token_tmp)

        # execute file-based modules
        results_file = self._handle_file(self.logfile)

        if results_line:
            # report the results, if any
            self.results['line'] = results_line

        if results_token:
            # report the results, if any
            self.results['token'] = results_token

        if results_file:
            # report the results, if any
            self.results['file'] = results_file

    def get_results(self):
        # parse end results

        print(json.dumps(self.results, indent=3))

    def _handle_file(self, inputfile):
        # handle the file with file modules

        results = list()

        for module in self.modules['file']:
            if self.modules_filter and module.name not in self.modules_filter:
                continue

            result = module.run(inputfile)

            if result:
                results.append({module.name: result})

        return results

    def _handle_line(self, line):
        # handle the line with line modules

        results = list()

        for module in self.modules['line']:
            if self.modules_filter and module.name not in self.modules_filter:
                continue

            result = module.run(line)

            if result:
                results.append(module.name)

        return results

    def _handle_tokens(self, line_tokenised):
        # handle the individual tokens

        results = dict()

        for token in line_tokenised:
            for module in self.modules['token']:
                if self.modules_filter and module.name not in self.modules_filter:
                    continue

                result = module.run(token)

                if result:
                    if module.name not in results.keys():
                        results[module.name] = list()

                    results[module.name].append(token)

        return results

    def _remove_newlines(self, string):
        # remove newlines from string
        return re.sub('\n', '', string)
