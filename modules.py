import sys
import os
import imp
import traceback


class Modules:
    """
    Class handles operations with the modules in ./modules_dir (register, list)
    """

    DEFAULT_MODULES_DIR = "./modules_dir/"

    def __init__(self, config):
        self.modules = {
            'file': [],  # file detection modules
            'line': [],  # line detection modules
            'token': []  # token detection modules
        }

        self.config = config

        # register modules from constructor
        self._register_modules()

    def _register_modules(self):
        # register available modules

        if 'modules_dir' in self.config:
            modules_dir = self.config['modules_dir']
        else:
            modules_dir = self.DEFAULT_MODULES_DIR

        for module_file in os.listdir(modules_dir):
            if not module_file.endswith('.py'):
                continue

            try:
                module_tmp = imp.load_source('Module', '{0}{1}'.format(modules_dir,module_file))
                if not getattr(module_tmp, 'Module', None):
                    # skip this module
                    continue

                obj = module_tmp.Module(self.config)

                if getattr(obj, 'run', None):
                    # we have a module to register
                    self.modules[obj.type].append(obj)

            except:
                print('Could not load the module found')
                print(traceback.format_exc())
                sys.exit(-1)

    def list_modules(self):
        # list available modules

        for module_type in self.modules.keys():
            for mod_object in self.modules[module_type]:
                print(
                    '[  {0} -  {1}  ]: {2}'.format(mod_object.name.ljust(15), module_type.ljust(5),
                                                   mod_object.description))

    def get_config(self):
        # return configured modules
        return self.modules
